"use strict";

// exponent operator

// old way
// alert(Math.pow(2,8)); // 256

//es6
// alert(2**8) //256

//const keyword

// if (true){
//     var cohort = "charlie";
// }

// if (true){
//     const cohort = `charlie`;
// }

// var x = 10; // x is 10
// {
//     const x = 2 // x is 2
// }
// console.log(x)  // x is 10


// for...of

// SYNTAX:

// for (const element of iterable){

// }

// ex:
//  const y = [10, 20, 30];

 // for (const value of y){
 //     console.log(value);
 // }

// Arrow functions

// SYNTAX OF JS

// var sayHello = function (cohort) {
//     return `Hello from` + cohort;
// };

// SYNTAX FOR ES6
//  const sayHello = (Delta) => `Hello from` + cohort;
//  const sayHello = cohort => `Hello from` + cohort;


//
//SYNTAX JS

// var x = function (x,y) {
//     return x * y
// };


//SYNTAX ES6

// const x = (x,y) => x * y;

// DEFAULT FUNCTION PARAMETER VALUES
// SYNTAX JS

// function greeting(cohort) {
//     if (typeof cohort === `undefined`){
//         cohort = `World`
//     }
//
//     return console.log("Hello," + cohort)
//
// }
// console.log(greeting()) // Hello, World
//
// console.log(greeting("Echo"))
// SYNTAX FOR ES6

const greeting = (cohort = `World`)=> console.log(`Hello from ${cohort}`);

// OR

function greeting(cohort = `World`) {
    return console.log("Hello," + cohort);
};

//greetin();    gives us Hello,World
// greeting(`Charlie`);  gives us Hello, Charlie


// OBJECT PROPERTY

// JS

// var person = {
//     name: `Charlie`,
//     age: 3
// }

// ES6

// const name = `Charlie`;
// const age = 3;
// const person = {
// name,
// age,
// };

// OBJECT DESTRUCTURING

// JS

// var person = {name:`charlie`, age: 3};

// var name = person.name;
// var age = person.age

// console.log(name);  gives us Charlie
// console.log(age); gives us 3

//ES6

// const person = {name:`Charlie`, age:3};
// const {name, age} = person;

// console.log(name);   gives us Charlie
// console.log(age); gives us 3

// OBJECT DESTRUCTURING ES6

// function personInfo(person){
// var name = person.name;
// var age = person.age;
// console.log(name);  gives us Charlie
// console.log(age);  gives us 3
// };

// const person = {name:`Charlie`, age:3};
// personInfo(person)

// DESTRUCTURING ARRAYS

//const myArray = [1,2,3,4,5];
// const [x,y,thirdIndex,bob,karen]= myArray;
// console.log(x); gives us 1
// console.log(y); gives us 2
// console.log(bob);  gives us 4


// map, filter and reduce

var officers = [
    {id: 20, name: `Captain Piett`},
    {id: 24, name: `General Veers`},
    {id: 56, name: `Admiral Ozzel`},
    {id: 88, name: `Commander Jerjerrod`}
];

// we need [20, 24, 56, 88]

// using a .forEach

// var officersIds[];
//
// // using a .forEach to push the id property of each officer
// officers.forEach(function (officer) {
//     officersIds.push(officer.id);
// });
// console.log(officersIds);

// var mapOfficerIds = officers.map(function (officer) {
//     return officer.id;
// })
// console.log(mapOfficerIds);

// var pilots = [
//     {
//         id: 2,
//         name: "Wedge Antilles",
//         faction: "Rebels"
//     },
//     {
//         id: 8,
//         name: "Ciena Ree",
//         faction: "Empire"
//     },
//     {
//         id: 40,
//         name: "Iden Versio",
//         faction: "Empire"
//     },
//     {
//         id: 66,
//         name: "Thane Kyrell",
//         faction: "Rebels"
//     }
// ];
//
// var rebels = pilots.filter(function (pilot) {
//     return pilot.faction === "Rebels";
// });
//
// var empire = pilots.filter(function (pilot) {
//     return pilot.faction === "Empire";
// });


var pilots = [
    {
        id: 41,
        name: "Poe Dameron",
        years: 14
    },
    {
        id: 2,
        name: "Temmin `snap` Wexley",
        years: 30
    },
    {
        id: 41,
        name: "Tallissan Lintra",
        years: 16
    },
    {
        id: 99,
        name: "Ello Asty",
        years: 22
    }
];


var totalYears = pilots.reduce(function (acc, pilot) {
    return acc + pilot.years
}, 0);





















































