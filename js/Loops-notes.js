// WHILE LOOP

// var number = 0;
// while (number <= 100){
//     console.log(`# ${number}`);
//
//     // increment the value by 3...
//     number += 3;
// }

// Do-while loop

// var x = 10;
//
// do {
//     console.log(`do-while loop # ${x}`);
//
//     // decrement the value of 1
//     x--;
//
//     // x-=1;
// } while (x >= 0);


// for loop

// for (var x = 100; x <= 200; x += 10){
//     console.log("for loop # " + x);
// }


// break and continue

// var endAtNumber = 5;
//
// for (var i = 1; i <= 100; i++){
//     console.log(`loop count # ${i}`);
//
//     //if statement
//     if (i === endAtNumber){
//         alert("We have reached our break!");
//         break;
//         console.log("Do you see this message?");
//     }
// }


// ** continue**

// for (var n = 1; n <= 100; n++){
//     // if statement
//     if (n % 2 !== 0){
//         continue;
//     }
//       console.log("Even number: " + n );
//
// }


// using a while loop, create the output shown below:
/*
2
4
8
16
32
64
128
256
512
1024
2048
4096
8192
16384
32768
65536
 */
 var num = 2;
 while (num <= 65536){
     console.log(num);
     num *= 2;
 }



// var num = 2; 
// while (num <= 65536){
//     console.log(num);
//     num *= 2;
// }

// other shortcuts 
// number += 2;
// number -= 2;
// number /= 2;
