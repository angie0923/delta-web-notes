function isOdd(x) {
    console.log(x % 2 == 1);    
}
isOdd(19);

function isEven(x) {
      console.log(x % 2 == 0);
}
isEven(5);

function isSame(input) {
       var num1 = 10
       console.log(num1 === input);
}
isSame(10);
 
function isSeven(input) {
    console.log(input === 7);     
}
isSeven(7);
 
function addTwo(x) {
        console.log(x + 2);
}
addTwo(4);

function isMultipleOfTen(x) {
         console.log(x % 10 === 0);
}
isMultipleOfTen(100);
 
function isMultipleOfTwo(x) {
          console.log(x % 2 === 0);
} 
isMultipleOfTwo(6);

function isMultipleOfTwoAndFour(x) {
          console.log(x % 2 === 0 && x % 4 === 0);
}
isMultipleOfTwoAndFour(12);

function isTrue(input) {
         console.log(input === "true");
}
isTrue("true");

function isFalse(input) {
         console.log(input === "false");
}
isFalse("true")

function isVowel(input) {
        console.log(input === "a" || input ==="e" || input === "i" || input === "o" || input === "u" 
        || input === "A" || input === "E" || input === "I" || input === "O" || input === "U");
}
isVowel("I")

function isTriple(str, num) {
         console.log(str.repeat(num));
}
isTriple("7", 3);

function isQuadNum(str,num) {
          console.log(str.repeat(num));
}
isQuadNum("7",4);

function degreesToRadians(degrees) {
          var pi = Math.PI;
          console.log(degrees * (pi/180));
}
degreesToRadians(4);

function reverseString(str) {
          var splitString = str.split("");
          var reverseArray = splitString.reverse();
          var joinArray = reverseArray.join("");
          console.log(joinArray);
}
reverseString("Hello");


 

