"use strict";
/*
What is jQuery?

"Write less, do more"
    a lightweight JavaScript
    Fast, small, and feature-rich JavaScript library
    Simplifies HTML DOM traversal and manipulation
    can also simplify CSS animation


Why JQuery?

    while there are many JavaScript libraries out there, JQuery is the most
    extendable.

    companies that use jQuery:
    google
    microsoft
    netflix


the jQuery object

    We will be using the jQuery object to find and create HTML elements
    from the DOM.

    The jQuery object is defined in the API documentation:
        return a collection of matched elements either found in the DOM
        based on passed argument(s) or created by passing and HTML string.

in jQuery, we commonly use the dollar sign $, an alias of jQuery to reference
the jQuery object, as we will be seeing.....

DOCUMENT READY

JavaScript provides the ability to natively determine if the window has finished
loading using the window.onload event handler.

 */

// jQuery Document Ready
// Passing an anonymous function in ready() will execute our code when the document
// is ready for JavaScript manipulation:

// $(document).ready(function () {  // to use the document ready event listener, we chain the ready() method to the document.
//  alert("This page has finished loading....")
// });

// JavaScript
// window.onload = function () {
//    alert("this page has finished loading...")
// }

// jQuery makes selecting elements in the DOM simple :

// Manipulating Elements
// jQuery selectors may look similar to CSS selectors, because it is!

// syntax: $("selector")
/*
a few things to take note of:
    the selector is inside of a string
    the string is inside of $(), which is calling a function named $
    This will return a jQuery object
 */


// ID selector
// the syntax for selecting an element by id is:
    // $("#some-id-name");

// var content = $("#codebound").html();   // .html() -> similar to .innerHTML
    // console.log(content);

/*
Here ^ we used the jQuery alias `$` and an id selector to match the id `codebound`.

We chained the `html()` method to get the contents of the HTML element as
a string.

Then assigned that string to the variable `content`

Alerting the `content` variable shows us the text inside the heading
element with the id of `codebound`.
 */


/*
.html() & .css():

two basic methods to know right now:
    .html()--returns the html content(s) of the selected element(s)
        similar to the ".innerHTML" property
    .css() allows us to change css properties for the selected elements
        similar to the ".style" property
 */
// CSS Selector | .css -> similar to .style property

// targets a single property

// $(".urgent").css("background-color", "red");





// Multiple style properties

// $(".not-urgent").css(
    // {
        // "background-color": "yellow",
        // "text-decoration": "line-through"
    // }
// );

// MULTIPLE SELECTOR

// $(".urgent, p").css("color", "blue");


// ALL SELECTOR

// $("*").css("background-color", "lightBlue");

// ELEMENT SELECTOR

// $("h1").css("text-decoration", "underline");

// $("*").css("border", "1px solid orange");

// $("header").addClass("headline");
// $("li").on("click", function () {
//   $(this).remove();
// });

/*
jQuery Events:

jQuery it is easier to work with different types of events. there's many
events but we'll look at:
.click() - binds an event handler to the "click" JavaScript
.dblclick() - binds an event handler to the "dblclick" JavaScript
.hover() - binds two handlers to the matched elements, to be executed
when the mouse pointer enters and leave the element

Recall:
- An event listener, or event handler function is a callback function that is called when an event
happens.
- The handler function is passed the event object when it is called
- The event object contains details about the event that occurred, as well as methods for modifying
the event, for example, preventDefault
- The event object can be omitted from the function definition if it is not used
- We can attach an anonymous function or a named function as a callback function
- Any anonymous function can be refactored to ba a named function.

Event Handler Functions - JavaScript
1. We can attach an anonymous function or
a named function as a callback function



Now let’s see how we can shorten these codes
up by using jQuery.


.click()

The syntax for using .click() is: $( selector ).click( handler )
Example
First, at the following in an html file:

<h3 id = "codebound">Hello from codebound! </h3>

Then in your javascript file, we’ll use jQuery to attach an event listener that’ll run when the h3 with the id
of ‘codebound’ is clicked

$("#codebound").click(function(){
alert("h3 with id codebound was clicked!");
});

On your webpage, click on the h3 to the alert


.dblclick()
The usage of .dblclick() is the same as .click(), but as the name implies, the event will be
triggered when the element is, what-for-it… double clicked!
    Take the same example for our html:

    <h3 id = "codebound">Hello from codebound! </h3>

    Update our javascript file:

    $("#codebound").dblclick(function(){
alert("h3 with id codebound was double clicked!");
});

    Clicking on the h3 will now do nothing, but double clicking will show an alert box


.hover()
The .hover event handler combines two other event handlers: mouseenter and mouseleave.
So .hover will take in two different callback functions that will be called when the mouse
enters the element, and when it leaves the element.

The syntax for .hover() is:

$( selector ).hover( handlerIn, handlerOut ) → handlerIn is handler in
T
his is the same as attaching both handlers separately:

$( selector ).mouseenter( handlerIn )
$( selector ).mouseleave( handlerOut )



.hover() example

Use the .hover() event handler to change the background color of our h3 element.

Add the following in your JavaScript:

$("#codebound").hover(
  function (){
    $(this.).css("background-color", "skyblue");
    },
  function (){
    $(this.).css("background-color", "white");
    }
 );

Here we are using $(this) to reference the
selected DOM element inside our .hover() method.
When we are inside of a callback function for an
event, $(this) will always refer to the selected DOM
element that triggered the event.
*/


$("#codebound").click(function () {
  alert("h3 with id codebound was clicked!");
});

$("#one").click(function () {
  alert("li with id one was clicked!");
});

$("#header").click(function () {
  alert("h1 with id of header was clicked!");
});

$("#header").dblclick(function () {
  alert("h1 with id of header was dblclicked!");
});


$("h1").hover(
    function () {
      $(this).css("background-color", "lightBlue");
    },
    function () {
      $(this).css("background-color", "white");
    }
);


$("#four").hover(
    function () {
      $(this).css("background-color", "cyan");
    },
    function () {
      $(this).css("background-color", "white");
    }
);


/*

Keyboard Events

jQuery events includes keyboard events like a key being pressed or when a key is released
after being pressed.

We will be focusing on the following methods:
- .keydown() - bind an event handler to the “keydown” event on an element
- .keypress() - bind an event handler to the “keypress” event on an element
- .keyup() - bind an event handler to the “keyup” event on an element


.keydown()

*****The syntax for .keydown() is:
$( selector ).keydown( handler )******


We can use the .keydown() event handler the same way we do with a click:

<p>Please type something in this form</p>
<form>
  <input type="text" id="my-form" placeholder="Enter text here">
</form>

$("#my-form").keydown(function(){
  alert("Hey! You pushed down a key and just released it!")
});
*/

$("#my-form").keydown(function(){
  alert("Hey! You pushed down a key and just released it!")
});

/*

Attribute Methods

These methods get and set DOM attributes of elements. Here’s the most
commonly used methods:

- .html()
- .css()
- .addClass() - adds the specified class(es) to each of the set of matched elements
- .removeClass() - removes single class, multiple or all classes from each element in the set
of matched elements.
- .toggleClass() - adds or removes one or more classes from each element in the set of
matched elements, depending on either the class’ presence or the value of the switch
argument.

Change Content with .html()

The .html() method can be used to get the current html of an element, but it can
also be used to change or set the content of that specific element.

Html:

<h3 id = "codebound">Hello from codebound! </h3>



JavaScript:

$("#codebound").click(function(){
  $(this).html("Go CodeBound!");
});

Now when the element with the id of codebound is clicked, it’s html will be changed to “Go CodeBound!”.
*Another method similar to .html is .text.
 */

// $("#codebound").click(function(){
//   $(this).html("Go CodeBound!");
// });

/*

.addClass()

Adding classes dynamically to elements is a great way to control the styling of a page and
use event handlers to add functionality. The .addClass method allows us to do this.

********The syntax for .addClass is:
.addClass( className )  **********


*className would be a string that contains the name of the class to be added.


.addClass() Example

HTML:
 <h1>this is the first heading</h1>

 <p>this is a paragraph</p>
 <p>this is another paragraph</p>

CSS:

 .change-heading {
      font-size: 20px;

 JavaScript:

 $("h1).click(function () {





- In the html, we have an h1 element, and two paragraph elements
- In the css, we create a class name change-heading, where we have some custom styling
- In the js, using jQuery, we added a click event to the h1 element. Then we see the .addClass()
method being used to add the change-heading class to the h1 element.
- Clicking the h1 element will change the style of “This is the first heading”






 */









