
// add an effect that will fade out the movie
// list in 5 seconds

// $("#movie-list-toggle").dblclick(function () {
//   $("#movies").fadeOut(5000);
// });


// Using jquery, hide the list of shows

// $("#shows-list-toggle").click(function () {
//  $("#shows").hide();
// });

// using jquery, show the list of shows
// after the user hovers over "Shows" in the
// web page.


// $("#shows-list-toggle").hover(
//     function () {
//        $("#shows").show();
//     },
//     function () {
//        $("#shows").hide();
//     }
// )


// In the html, create an element that contains
// "This message appeared after 5 seconds.

// Using jQuery, make the message appear after 5 seconds,
// when the user hovers onto the document.

$("p").hide();

$(document).hover(function () {
   $("p").fadeIn(5000);
})

// $("#message").hover(
//     function () {
//        $("#message").hide();
//     },
//     function () {
//        $("#message").show(5000);
//     }
// )









