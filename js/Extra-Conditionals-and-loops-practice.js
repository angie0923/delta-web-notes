// EXTRA CONDITIONALS AND LOOPS PRACTICE
// =====================================


// Write a JavaScript program that accepts two integers and displays the larger one.

// function greaterThan(input1, input2) {
//      if (input1 > input2){
//          console.log(input1);
//      }
//      else {
//          console.log(input2);
//      }
// }
// greaterThan(9,3);


/* Write a JavaScript conditional statement to sort three number.
Display an alert box to show the result
Sample numbers: 0, -1, 4
Output: 4, 0, -1
*/

// function sort(a, b, c) {
//       if (a < b && b < c) {
//           alert(a, b, c);
//       }
//       else if (a < c && c < b){
//           alert(a, c, b);
//       }
//       else if (b < a && b < c){
//           alert(b, a, c);
//       }
// }
// sort(1, 2, 3);



/* Write a JavaScript conditional statement to find the largest of five numbers.
Display an alert box to show the result
Sample numbers: -5, -2, -6, 0, -1
Output: 0
*/


// a = -5;
// b = -2;
// c = -6;
// d = 0;
// f = -1;
//
// if (a > b && a > c && a > d && a >f){
//     alert(a);
// }
//
// else if (b > a && b > c && b > d && b > f){
//    alert(b); 
// }
//
// else if (c > a && c > b && c > d && d > f){
//     alert(c);
// }
//
// else if (d > a && d > b && d > c && d > f){
//     alert(d);
// }
//
// else {
//     alert(f);
// }


/* Write a JavaScript program to construct the following pattern, using a nested for loop
*
**
***
****
*****
*/

// var a, b, cde;
// for (a = 1; a<= 6; a++){
//     for (b = 1; b < a; b++){
//         cde = cde + ("*");
//     }
//     console.log(cde);
//     cde = ``;
// }



/* Write a JavaScript program which iterates the integers from 1 to 100.
For the multiples of 3 print "Fizz" instead of the number.
For the multiples of 5 print "Buzz" instead of the number.
For the multiples of 3 AND 5 print "FizzBuzz".
*/

// for (i = 1; i<100; i++) {
//     if (i % 3 == 0) {
//         if (i % 5 == 0) {console.log('fizzbuzz')}
//         else {console.log('fizz')}}
//     else if (i % 5 == 0) {console.log('buzz')}
//     else {console.log(i)};
// }




// Write a JavaScript program to sum the multiples of 3 and 5 under 1000.

// var sum = 0;
// for (var i = 0; i < 1000; i++) {
//     if (i % 3 === 0 || i % 5 === 0) {
//         sum += i;
//     }
// }
// console.log('Sum: %d', sum);
