// ** CONDITIONALS STATEMENTS**

//if statement
/*
SYNTAX:
 if (condition) {
    code will execute if condition is met
    }
 */

// example

// var numberOfLives = 1;  //nothing happens
// var numberOfLives = 0;                    
// if(numberOfLives === 0);{
//     alert("Game over!");
// }
//
// if/else statement

/*

SYNTAX:
    if (condition) {
    code executed if condition is met
    }

    else {
        code executed if 1st condition is not met
    }

 */

// example

// var score = 8;
//
//     if (score > 10) {
//         console.log("Nice job, you scored more than 10");
//     }
//     else {
//         console.log("Try again... loser");
//     }
//
//
// if / else if / else statement

/*
SYNTAX:
    if (condition) {
        code executed if condition is met
        }
    else if (condition2) {
        code executed if condition is false and condition2 is true.
        }
     else {
        code executed if both conditions are not met 
        }
        
 */
        
//
// var age = 11;
//
//     if (age  > 18 && age < 21) {
//         console.log("Welcome to the club, don't drink!");
//     }
//
//     else if (age >= 21) {
//         console.log("Welcome ot the club, don't get drunk!");
//     }
//
//     else {
//     console.log("Beat it kid!");
//     }
//                  


// var input = prompt("What's your favorite fast food?");
//
//     if (input === "Pizza" || input === "pizza"){
//         console.log("Cowabunga");
//     }
//     else if (input === "Hamburger" || input === "hamburger"){
//         console.log("Welcome to Good Burger");
//     }
//     else if (input === "Wings" || input === "wings"){
//         console.log("Add some hot sauce onto that");
//     }
//     else {
//         console.log("Eww");
//     }


// ** SWITCH CASE **

// another method for writing conditinal statements

// var color = "Red";
//
// switch ( color ){
//     case "Red":  //COLON goes here
//         alert("You chose Red!");
//         break;
//        
//     case "Blue":
//         alert("You chose Blue!");
//         break;
//        
//     case "Green":
//         alert("You chose Green!");
//         break;
//
//     default: 
//         alert("You didn't select a cool color!");
//         break;
//
// }


// function seasonCase(Month) {
//     switch (month){
//         case "December":
//             alert("Winter is here!");
//             break;
//            
//         case "January":
//             alert("Winter is here!");
//             break;
//            
//         case "February":
//             alert("Winter is here!");
//             break;
//            
//         case "March":
//             alert("Spring is here!");
//             break;
//            
//         case "April":
//             alert("Spring is here!");
//             break;
//            
//            
//     }
// }
//
//
// seasonCase("February");



















