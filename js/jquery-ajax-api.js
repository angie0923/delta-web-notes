"use strict";

// load users in our console log

$.get("https://jsonplaceholder.typicode.com/users",{


}).done(function (data) {
    // // what do we want to do with the data...?
    // console.log(data);
    //
    // // get Nicholas Runolfsdottir V in the console log.....
    // console.log(data[7].name);
    //
    // // get "Gwenborough" from the first object of the array...
    // console.log(data[0].address.city);

//    create a variable and assign it to our function "display users"

    var renderData = displayUsers(data);

//    target our id "users" and append renderData variable
    $("#users").html(renderData);


});



function displayUsers(users) {
//    local variable and assign to an empty string

    var usersOnHTML = "";

//    Loop through the array

    users.forEach(function (user) {
    //  call the local variable, and dynamically create the html
         usersOnHTML += `
        <div class="user">
            <h3>Employee Name:  ${user.name}</h3>
        
            <a href="http://${user.website}">View website</a>
            
            <p>User Info:</p>
            
            <ul>
                <li>
                    <p>Username: ${user.username}</p>
                </li>
                <li>
                    <p>Address: ${user.address.street} ${user.address.city}</p>
                </li>
            </ul>
        </div>
        `
    })

//    return statement to return our variable "usersOnHTML"
    return usersOnHTML;
}













