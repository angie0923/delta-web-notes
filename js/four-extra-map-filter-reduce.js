"use strict";

const companies = [
    {name: 'Walmart', category: 'Retail', start_year: 1962, location: { city: 'Rogers', state: 'AR' }},
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];

// filter (should be map) all of the company names

const namesFiltered = companies
    .map(company => company.name)
console.log(namesFiltered);


// filter all the company names started before 1990
// var years = companies.filter(function (company) {
//     return company.start_year < 1990
// })
// console.log(years);      (This gives entire object)


// this one gives us just the names
const years = companies.filter(c => c.name && c.start_year < 1990);
const companyNames90 = years.map(company => company.name);
console.log(companyNames90)



// filter all the retail companies

// var retail = companies.filter(function (company) {
//     return company.category === "Retail"
// })
// console.log(retail);

const retail = companies.filter(company => company.category === "Retail")

// find all the technology and financial companies

// var technology = companies.filter(function (company) {
//     return company.category === "Technology"
// });
// console.log(technology);
//
// const financial = companies.filter(function (fine) {
//     return fine.category === 'Financial'
// });
// console.log(financial)


var techfin = companies.filter(company => company.category === "Technology" || "Financial");
console.log(techfin);

// filter all the companies that are located in the state of NY

// var ny = companies.filter(function (nyc) {
//     return nyc.location.state === 'NY'
// })
// console.log(ny);

const newYork = companies.filter(company => company.location.state === "NY")
console.log(newYork)

// find all the companies that started on an even year.

// var evensFiltered = companies.filter(function (c) {
//     return c.start_year % 2 === 0;
// })

// var evensFiltered1 = companies.filter(c => c.start_year % 2 === 0)
// console.log(evensFiltered);

// use map and multiply each start year by 2

// const multiply = companies.map(companies => companies.start_year * 2)
// console.log(multiply);

// find the total of all the start year combined.

// const startYear = companies.reduce((acc,year) => acc + year.start_year, 0);
// console.log(startYear);

// display all the company names in alphabetical order

// const alphabetical = companies.map(company => company.name);
// console.log(alphabetical.sort());

// display all the company names from youngest to oldest.

const youngest = companies.map(company => company.name && company.start_year);
console.log(youngest.sort());

const reverseYoungest = youngest.reverse();
console.log(reverseYoungest);


const youngToOld = companies.map(function (company) {
   return company.start_year + "" + company.name;
})
console.log(youngToOld.sort().reverse());










































































































