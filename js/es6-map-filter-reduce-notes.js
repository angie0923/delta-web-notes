"use strict";

/*
Map, Filter, Reduce

map- transform each element in the collection
filter- filter the values
reduce- reduce a collection to a single value
 */

var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];

// var evens =[];
// var odds = [];

// js for loop

// for (var i = 0; i < numbers.length; i++){
//     //find all even numbers
//     if (numbers[i] % 2 === 0) {
//
//         // add the index from numbers to evens
//         evens.push(numbers[i]);
//     }
//     else {
//         // add the index from numbers to odds
//         odds.push(numbers[i]);
//     }
//
// }
//
// console.log(evens);
// console.log(odds);

// same thing but with .filter

// var evensFiltered = numbers.filter(function (n) {
//     return n % 2 === 0;
// })
// console.log(evensFiltered);


// var oddsFiltered = numbers.filter(function (n) {
//     return n % 2 !== 0;
// })
//
// console.log(oddsFiltered);



// var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];

// Map
// write code that produces a new array of numbers with each number multiplied by 10

// var numbersTimesTen = numbers.map(function (num) {
//     return num * 10;
// });
//
// console.log(numbersTimesTen);


// const numbersTimesTen = numbers.map(num => num * 10);
// console.log(numbersTimesTen)


// REDUCE
const myArray = [1, 2, 3, 4, 5];


// const sum = myArray.reduce(function (acc,currentNumber) {
//   return acc + currentNumber;
// }, 0);
//
// console.log(sum);


// same example converted to arrow function

// const sum = myArray.reduce((acc, currentNumber)=> acc + currentNumber, 0);
//
// console.log(sum);

// if the array is changed to a string it concatenates the answer


// create an array of objects

const officeSales = [

    {
        name: "Jim Halpert",
        sales: 500
    },
    {
        name: "Dwight Schrute",
        sales: 750
    },
    {
        name: "Ryan Howard",
        sales: 150
    }
];

// using a reduce, get the total amount of sales for the week

const money = officeSales
    .reduce((acc, person)=> acc + person.sales, 0);

console.log(money);













