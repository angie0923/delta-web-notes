/**
 * TODO: Change the body's background color to skyblue
 */
var body = document.getElementById("body");

body.style.backgroundColor = "skyblue";

// OR   document.body.style.backgroundColor = "skyblue";
/**
 * TODO: Console log the section with an id of container
 */

var container = document.getElementById("container");
console.log(container);


/**
 * TODO: Console log the section with an id of container using querySelector. What is a querySelector?
 */

//The querySelector() method returns the first element that matches a specified CSS selector(s) in the document.

// Note: The querySelector() method only returns the first element that matches the specified selectors.
// To return all the matches, use the querySelectorAll() method instead.

var container1 = document.querySelector("#container");

console.log(container1);

// OR   *** console.log(document.getElementById("container");   **

/**
 * TODO: Give the div with the class of header the text of "Hello World"
 */

var header = document.getElementsByClassName("header");
header.item(0).innerHTML = "Hello World";

// OR document.getElementByClassName("header").item(0).innerHTML = "Hello World!";

/**
 * TODO: Console log all of the list items with a class of "second"
 */

var second = document.getElementsByClassName("second");
console.log(second);

/**
 * TODO: Change the font family of all of the list items with a class of "second"
 */

var second = document.getElementsByClassName("second");
second.item(0).style.fontFamily = "Fantasy";
second.item(1).style.fontFamily = "Fantasy";


// OR
// for (var i = 0; i < second.length; i++ ){
//     second.item(i).style.fontFamily = "Fantasy";
// }


/**
 * TODO: Give the section with an id of container the text of "Go CodeBound!"
 */

var container = document.getElementById("container");
 container.innerHTML += "Go CodeBound!";


 /**
 * TODO: Create a new li element, give it the text "four", append it to the ul element
 */
// var list = document.getElementById("list");
// // var four = document.getElementById("four").value;
// var entry = document.createElement("li");
// // entry.appendChild(document.createTextNode("four"));
// list.appendChild(entry);

var newItem = `<li class = "fourth">four</li>`;      // creates <li class = "fourth"
var uList = document.getElementsByTagName("ul");
uList.item(0).innerHTML += newItem;




// var entry = document.createElement("li");  
// var htmlFour = document.createTextNode("four");
//
// entry.appendChild(htmlFour);
// document.getElementById("list").appendChild(entry);
 
 /**
 * TODO: Give the div with an class of footer the text of "This is my Footer!"
 */
 
 var footer = document.getElementsByClassName("footer");
 
 footer.item(0).innerHTML = "This is my Footer!";

 
 //Bonus

var message = setTimeout(function () {
    alert("Welcome to the DOM Exercise!");
}, 5000);
