/* JAVASCRIPT ARRAYS */
// Complete the following in a new js file named arrays-exercise.js
"use strict";
/** TODO:
 *  Create a new EMPTY array named sports.**/

// var sports = [];
// console.log(sports);

/** Push into this array 5 sports**/

// sports.push("tennis", "football", "basketball", "soccer", "baseball");
// console.log(sports);

/** Sort the array.**/

// console.log(sports.sort());

/** Reverse the sort that you did on the array.**/

// sports.reverse();
// console.log(sports);

/** Push 3 more sports into the array **/

// sports.push("swimming", "hockey", "volleyball");
// console.log(sports);

/** Locate your favorite sport in the array and Log the index. **/

// var indexElement = sports.indexOf("swimming");
// console.log(indexElement);

//OR

// console.log(sports[5]);

/** Remove the 4th element in the array **/

// sports.splice(4, 1);
// console.log(sports)

/** Remove the first sport in the array **/

// sports.shift();
// console.log(sports);


/** Turn your array into a string  **/

// var rslt = sports.toString() ;
// console.log(rslt);

// OR
// console.log(sports.join(","));



/**
 * TODO: Create an array to hold your top singers/bands/etc.
 *  const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
 1. console log 'Van Halen'
 2. add 'Fleetwood Mac' to the beginning of the array
 3. add 'Journey' to the end of the array
 */

// var myFavoriteBands = ["AFI", "Smashing Pumpkins", "Metallica", "Megadeth",
// "Van Halen", "AC/DC", "Pink Floyd"];
// console.log(myFavoriteBands);
//
// console.log(myFavoriteBands[4]);
//
// myFavoriteBands.unshift("Fleetwood Mac");
// console.log(myFavoriteBands);
//
// myFavoriteBands.push("Journey");
// console.log(myFavoriteBands);






/** Create a new array named cohort_name that holds the names of the people in your class**/

// var cohort_name = ["Jose", "Victor", "Alyssa", "Angela"];
// console.log(cohort_name);

/** Log out each individual name from the array by accessing the index. **/

// console.log(cohort_name[0]);
// console.log(cohort_name[1]);
// console.log(cohort_name[2]);
// console.log(cohort_name[3]);


/** Using a for loop, Log out every item in the COHORT_NAME array **/

// for (var i = 0; i < cohort_name.length; i++){
//     console.log(cohort_name[i]);
// }

/** Refactor your code using a forEach Function **/

// cohort_name.forEach(function (Class) {
//      console.log(Class);
// });

/** Using a for loop. Remove all instance of 'CodeBound' in the array **/

// var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
// console.log(arrays1);

// for (var i = arrays1.length; i--;){
//      if (arrays1 [i] === "CodeBound"){
//          arrays1.splice(i,1);
//      }
// }                                        
// console.log(arrays1);

// OR

// for (var i = 0; i <= 5; i++) {
//     switch (arrays1 [i] == "CodeBound") {
//         case true:
//             console.log(arrays1);
//             arrays1.splice(i, 1);
//             break;
//     }
// }
// console.log(arrays1);

// OR

// for (var i = 0; i < arrays1.length; i++){
//     if (arrays1[i] !== "CodeBound"){
//         console.log(arrays1[i]);
//    
//     }
// }


/**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/

// var numbers = [3, '12', 55, 9, '0', 99];
//   
// var sum = 0;
//
// numbers.forEach(function (number) {
//         sum += parseInt(number);
//        
// });
// console.log(sum);



/**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
// var numbers = [7, 10, 22, 87, 30];
//
// console.log(numbers);
//
// numbers.forEach(function (number) {
//        console.log(number * 5);
// });



/** Create a function named removeFirstLast where you remove the first and last character of a string.**/

// function removeFirstLast(input) {
// var removeArray = input.split("");
//     removeArray.shift();
//     removeArray.pop();
//     return removeArray.join("");
//    
// }
//
// console.log(removeFirstLast("Burger"));





// Bonus
/** Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9). This will return a string of those numbers in a
 *  phone number format.
 *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123"
 */
/** Create a function that will take in an integer as an argument and Returns "Even" for even numbers and returns "Odd" for odd numbers **/
/** Create a function named reverseString that will take in a string and reverse it. There are many ways to do this using JavaScript built-in
 * methods. For this exercise use a loop.
 */
/**Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
 * 386543 => [3,4,5,6,8,3]
 */

