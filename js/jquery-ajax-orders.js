"use strict";


//created a variable and assign an ajax GET request to data/orders.json
var ajaxRequest = $.ajax("data/orders.json");

// Callback methods for handling responses
/*
.done()
.fail()
.always()
 */

ajaxRequest.done(function (data) {
    // gets the entire array of objects
    console.log(data);

    // gets the first object of the array
    // console.log("The first object of the array is: " + data[0].price);

    // get the value of "Cap"...

    // console.log(data[3].item);

    // get the value of "Alyssa"...

    // console.log(data[2].orderedBy);

    // get the value of 12345 ....

    // console.log(data[1].orderNum);

    // get the price property of the first object....
    // console.log(data[0].price);


    // create a local variable and assign the buildHTML() function to it.

    var dataToHTML = buildHTML(data);

    // target our id "orders" in the html, and render our data...

    $("#orders").html(dataToHTML)
});



// Create a function that will display our "data from our request
function buildHTML(orders) {
    // create variable and assign an empty string...
    var orderHTML = "";

    // loop through the array for each individual order
    orders.forEach(function (order) {
        orderHTML += "<section>";
        orderHTML += "<dl>";
        orderHTML += "<dt>" + "Ordered Item" + "</dt>";
        orderHTML += "<br>";
        orderHTML += "<dd>" + order.item + "</dd>";
        orderHTML += "</dl>";
        orderHTML += "</section";
    });
    return orderHTML;
}
    // template string example
//     orders.forEach(function (order) {
//         orderHTML += `
//         <h3>Ordered Item</h3>
//         <p>${order.item}</p>
//         <p> Ordered by: ${order.orderedBy}</p>
//         <p> Total Amount:$ ${order.price}</p>
//         `;
//     })
//

//
// }












