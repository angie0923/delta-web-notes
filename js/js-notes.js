// EXTERNAL JavaScript

//single line comment

/*
this
is
a
multi
line
comment
 */

//console.log()

// console.log(5 * 100);

//this is a STRING
// console.log("hello");

//this is a BOOLEAN --NO QUOTATIONS TRUE or FALSE only unless it has quotations

// console.log(true);
// console.log(false);

// DATA TYPES
// number, string, boolean, undefined, null... object
// used to declare any VARIABLES

// VARIABLES
// 3 ways to declare a variable
//let, const, var (most common used)

// DECLARING VARIABLES
// syntax - structure: var nameOfTheVariable;
//syntax - var nameOfTheVariable = value;

// DECLARE A VARIABLE NAMED firstName (camel casing)
var firstName;

// ASSIGN A VALUE OF "Billy" TO firstName
firstName = "Billy";

// CALL THE VARIABLE NAME
// WHAT IS THE NAME OF THE VARIABLE?
// console.log(firstName);

// DECLARE A VARIABLE NAMED age AND 
// ASSIGN A VALUE OF 30 TO AGE
// var age = 30;

// CALL THE VARIABLE NAME age
// console.log(age);

// OPERATORS
// mathematical arithmetic

/*
addition +
subtraction -
multiply *
divide /
modulus AKA remainder  %
 */

// console.log( 12 / 4 );
// console.log( 12 % 5);
// console.log( 12 % 4 )
// console.log( 1 + 2 * 4 / 5); 

// PEMDAS
// () ^ * / + -

// LOGICAL OPERATORS
/*
AND &&
OR  ||
NOT !

NOTE: returns a boolean value when used with boolean values
also used with non-boolean values

AND && ( one FALSE statement and the entire thing is FALSE)
TRUE STATEMENT && TRUE STATEMENT (true)
TRUE STATEMENT && FALSE STATEMENT (false)
FALSE STATEMENT && TRUE STATEMENT (false)
FALSE STATEMENT && FALSE STATEMENT (false)

 */
 // console.log(true && true);
 // console.log(true && false);
 // console.log(false && true);
 // console.log(false && false);
 
// OR || ( all we need is one TRUE statement and the entire thing is TRUE)

//  T || F ?   true
// console.log(true || false);  
// console.log(true || true);
// console.log(false || false);
// console.log(false || true);

// NOT ! 
// the opposite of the statement
// !true = false
// !false = true

// console.log(!false);
// console.log(!!!!!!!true);

// COMPARISON OPERATOR

// ==, === (strictly equal), <, >, <=, >=,

// = USED TO ASSIGN VALUES TO A VARIABLE

var lastName = "Smith";

// == checks if the VALUE is the same
// === checks if the VALUE AND DATA TYPE are the same


var entry = 12;
var entry2 = "12";

// console.log(entry === entry2);
// console.log(entry === entry2);

// !=, !===
// != checks if the value is NOT the same
// !== checks if the VALUE AND DATA TYPE are NOT the same
// console.log(entry != entry2);
// console.log(entry !== entry2);

// <, >, <=, >=

const age = 21;
                      
// console.log(age >= 21 || age < 18);
//             T      ||   F      = true

// console.log(age <= 21 || age >21);
//                 T  ||  T     =  true

// console.log(age < 21 && age > 18);
               // F     &&    T     =  false

// CONCATENATION

const person = "Bruce";
const person_age = 40;

console.log("Hello my name is " + person);
console.log("And I am " + person_age + "years old.");

console.log("Hello my name is " + person + " and I am " + person_age + "years old.");


// TEMPLATE STRINGS `back ticks` ${}

console.log(`Hello my name is ${person}.`);

console.log(`Hello my name is ${person} and I am ${person_age} years old.`);

// STRING METHODS
// HELPS US WITH STRING VARIABLES!

//.length ()
// GETS THE LENGTH OF A STRING

console.log(person.length); // 5

const greeting = "Hello World!";
console.log(greeting.length); //12 counts spaces and punctuation

// .toUpperCase() / .toLowerCase ()
// GET THE STRING IN UPPER CASE / LOWER CASE

console.log(greeting.toUpperCase());
console.log(greeting.toLowerCase());

// .substring()
// extracts characters from a string between a "start" and "end" 
// index -- not including the "end" inself

var goat = "Selena Quintanilla-Perez";
console.log(goat.substring(0, 6));  // Selena
// Quintanilla ?
console.log(goat.substring(7, 18)); // Quintanilla
console.log(goat.length);

// Perez ?
console.log(goat.substring(19));  // Perez

// FUNCTIONS
// a REUSABLE block of code that performs
// a specified task(s)

// SYNTAX FOR FUNCTION:
/*
function nameOfTheFunction() {
    code you want the function to do
    }
   
 */

// CREATED A FUNCTION named sayHello()
function sayHello() {   //inside the curly braces is the function's body
    console.log("Hello!");   // parameters go inside of the parenthesis
    alert("Hello!");
}

// CALL A FUNCTION
// when we want it to run, we call it by 
// it's name with the ()

// sayHello();



// CREATE A FUNCTION with PARAMATERS

function addNumber(num1) {
    console.log(num1 + 2);    
}

// CALL THE FUNCTION addNumber()
addNumber(13);

// NaN -- Not a Number

// CREATE A FUNCTION WITH TWO PARAMETERS

function message(name, age) {
    alert(`Hello my name is ${name} and I am ${age} years old.`);    
}

// CALL message() with the two parameters

message("Jose", 21);

// CREATE A FUNCTION WITH 3 PARAMETERS

function petInfo(name, animal, age) {
    console.log("My pet's name is " + name + ". He is a " + animal + ", and he is " + age + ".");
}

petInfo("Blue", "Cane Corso", 1);












