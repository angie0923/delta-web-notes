
(function () {

"use strict";

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY

// Exercise 1:
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

// const wait = (delay) => new Promise((resolve, reject) => {
//     setTimeout(() =>{
//         resolve(`You will see this after ${delay / 1000} seconds`)
//
//     }, delay);
// })
//
// wait(4000).then((data)=> console.log(data));



    
    
// Exercise 2:
// Write a function testNum that takes a number as an argument and returns a Promise that tests if
// the value is less than or greater than the value 10.
//     const testNum = new Promise((resolve, reject) => {
//         let num = 5
//         if ( num < 10) {
//             resolve("Less than")
//         }else {
//             reject("Greater than")
//         }
//
//     });
//     console.log(testNum);

    // const testNum = (num) => new Promise((resolve, reject) => {
    //     if (num > 10){
    //         resolve("Values is greater than 10");
    //     }
    //     else{
    //         reject("Value is less than 10");
    //     }
    //
    // })
    //
    // console.log(testNum(15))
// Exercise 3:
    //Write two functions that use Promises that you can chain! The first function, makeAllCaps(),
    // will take in an array of words and capitalize them, and then the second function, sortWords(),
    // will sort the words in alphabetical order. If the array contains anything but strings, it should
    // throw an error.

    let fish = ['goldfish', 'tuna', 'shark', 'puffer']

    const makeAllCaps = (words) =>
        new Promise ((resolve, reject) => {
            if (words.every(word => typeof word === 'string')){
                resolve(words.map(word => word.toUpperCase()))
            } else {
                reject(Error('No, the array you passed in contained an element that was not a string!'))
            }
        })

    const sortWords = (words) => {
        return words.sort((a, b) => {
            if (a > b){
                return 1
            } else {
                return -1
            }
        })
    }

    makeAllCaps(fish)
        .then(words => sortWords(words))
        .then(result => console.log(result))
        .catch(error => console.log(error))



})()