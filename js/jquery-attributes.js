
// $(document).ready(function () {
//
// });
//    $("#highlight-important").click(function () {
//        $(".important").addClass("highlighted");
//    });
// });

// syntax:  $("selector")

// $("#highlight-important").dblclick(function () {
//    $(".important").removeClass("important");
// });

// $("#highlight-important").click(function (e) {
//   e.preventDefault();
//   $(".important").toggleClass("highlighted");
//
// });


/*

event.preventDefault()
e.preventDefault()

stops the default behavior of the anchor element
from firing.


 */
// });

// Challenge:
// Convert the center alignment back to its
// normal alignment, using jQuery

// $("#highlight-important").dblclick(function () {
//    $(".important").removeClass("centered");
// });



