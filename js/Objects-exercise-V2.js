/* JAVASCRIPT OBJECTS */
// Complete the following in a new js file named objects-exercise.js
"use strict";
/**
 * TODO: Create an object called movie. Include four properties
 *  Console log each property.
 */

// var movie = {
//     title: "Scream",
//     releaseDate: "October",
//     protagonist: "Sidney Prescott",
//     antagonist: "Ghostface"
// }
// console.log(movie.title);
// console.log(movie.releaseDate);
// console.log(movie.protagonist);
// console.log(movie.antagonist);


/**
 * TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre
 *  console log each property.
 */

// var movie = {
//     title: "Spider-Man",
//     protagonist: "Spider-Man",
//     antagonist: "Green Goblin",
//     genre: "Action"
// };
// console.log(movie.title);
// console.log(movie.genre);
// console.log(movie.protagonist);
// console.log(movie.antagonist);


/**
 * TODO: Create a pet/companion object include the following properties:
 *  string name, number age, string species, number valueInDollars,
 *  array nickNames, vaccinations: date, type, description.
 */

// var pet = {
//     name: "Blue",
//     age: "1",
//     species: "Cane Corso",
//     value: "$5000",
//     nicknames: ["Boo", "Bootyroo", "Blueskiroodliedoo"],
//     vaccinations: {
//         date: "May 2020",
//         type: "Rabies",
//         description: "First vaccine in Rabies series."
// },    
// };

/**
 * TODO: Add 3 additional objects to the array below called movies. Be sure to include five properties: title, releaseDate, director, genre, and myReview.
 *  The myReview property should be a function
 */
//EX
const movies = [
    {
        title: 'Batman',
        releaseDate: {
            month: "June",
            date: 23,
            year: 1989
        },
        director: {
            firstName: 'Tim',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
        }
    },
    {
        title: "Spider-Man",
        releaseDate: {
            month: "July",
            date: "10",
            year: "1998"
        },
        director: {
            firstName: "David",
            lastName: "Montoya"
        },
        genre: ["Action", "Adventure"],
        myReview: function () {
           console.log("Spider-Man is one of my all time favorite movies!! I'll watch it again and again!!"); 
        }
    },
    {
        title: "Coraline",
        releaseDate: {
            month: "March",
            date: "5",
            year: "2010",
        },
        director: {
            firstName: "Tim",
            lastName: "Burton",
        },
        genre: ["Comedy", "Action", "Fantasy"],
        myReview: function () {
           console.log("I LOVE this movie!!! 4/4 Stars!  A MUST see!!"); 
        }
    },
    {
        title: "My Last Day on Earth",
        releaseDate: {
            month: "February",
            date: "18",
            year: "2021"
        },
        director: {
            firstName: "Angela",
            lastName: "Alexander"
        },
        genre: ["Horror", "Comedy"],
        myReview: function () {
           console.log("UGH!!!!  Is this weather EVER going to end????!!!!"); 
        }
    }
    ];
/** Once you added the additional movies, complete the following:
 *  Loop through the array to display and console log the 'Release Date: month / date / year'.
 *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, myReview'
 *
 * Loop through the array to display and console log the 'Genre'
 *
 *  Loop through the array to display and console log the 'Director: FirstName LastName'
 */

// movies.forEach(function (movie) {
//     console.log("Release Date:",movie.releaseDate);
// });

// movies.forEach(function (movie) {
//    console.log(movie.title);
//    console.log(movie.releaseDate);
//    console.log(movie.director);
//    console.log(movie.myReview());
// });

// movies.forEach(function (movie) {
//     console.log(movie.genre);
// });

// movies.forEach(function (movie) {
//    console.log("Director:",movie.director); 
// });





// BONUS
/** TODO:
 * HEB has an offer for the shoppers that buy products amounting to
 * more than $200. If a shopper spends more than $200, they get a 12%
 * discount. Write a JS program, using conditionals, that logs to the
 * browser, how much Stephen, Justin and Leslie need to pay. We know that
 * Justin bought $180, Stephen $250 and Leslie $320. Your program will have to
 * display a line with the name of the person, the amount before the
 * discount, the discount, if any, and the amount after the discount.
 *
 * Uncomment the lines below to create an array of objects where each object
 * represents one shopper. Use a foreach loop to iterate through the array,
 * and console.log the relevant messages for each person
 */
/*
var shoppers = [
        {name: 'Justin', amount: 180},
        {name: 'Stephen', amount: 250},
        {name: 'Leslie', amount: 320}
    ];
*/
