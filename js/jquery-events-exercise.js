// Add jQuery code that will change the background color of a 'h1' element when clicked.

$("h1").click(function(){
  $(this).css("background-color", "lightBlue");
});

// Make all paragraphs have a font size of 18px when they are double clicked.

$("p").dblclick(function () {
  $(this).css("font-size", "18px");
});

// Set all 'li' text color to green when the mouse is hovering,
// reset to black when it is not.

$("li").hover(
    function () {
        $(this).css("color", "green");
    },
    function () {
        $(this).css("color", "black");
    }
);

// Add an alert that reads "Keydown event triggered!" everytime a user pushes a key down

// $("#myForm").keydown(function () {
//   alert("KEYDOWN EVENT TRIGGERED!!!!");
// });

// OR

// $(document).keydown(function () {
//   alert("keydown event triggered!!")
// })

// Console log "Keyup event triggered!" whenever a user releases a key.

// $("#myForm").keyup(function () {
//   console.log("KEYUP EVENT TRIGGERED!!!!!!");
// });

// OR

// $(document).keydown(function () {
//   alert("keydown event triggered!!");
// });

// BONUS: Create a form with one input with a type set to text.
// Using jQuery, create a counter for everytime a key is pressed.

// $("#myForm").keydown(function () {
//   $("#output").html(function (i, val) {
//    return +val +1});
// });

// OR

$(document).keydown(function () {
   $("#output").html(function (i, val) {
       return +val +1});

});


