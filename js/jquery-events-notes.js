// MOUSE EVENT IN JAVASCRIPT

// var eventElement = document.getElementById("#delta-form");
//
// eventElement.addEventListener("click", function () {
//     execute code
// })

// SYNTAX:  $("selector").click( function );

// $("#delta").click(function () {
//    alert("The element h1 was clicked!");
// });

// $("#delta").dblclick(function () {
//     alert("The element h1 was double clicked!");
// });

// change the h1 font style when the mouse hovers over the text

$("#delta").hover(
    function () {
    $(this).css("font-family", "luminari");
    },
    function () {
    $(this).css("font-family", "fantasy");
    }
);

// KEYBOARD EVENTS

// SYNTAX: $("selector").keydown(handler function);

// $("#delta-form").keydown(function () {
//   alert("Hey! You pushed down a key in the form");
// });

$("#delta-form").keyup(function () {
    alert("Hey! You just released a key in the form");
});



