//  get main header element by id

var mainHeader = document.getElementById("main-header");

console.log(mainHeader); //HTML structure

console.log(mainHeader.innerHTML); //Hello World!

// set the inner HTML of mainHeader to "Winter is Gone!!"

mainHeader.innerHTML = "Winter is Gone!";

var  subHeader = document.getElementById("sub-header");

subHeader.style.color = "blue";

var listItems = document.getElementsByTagName("li");



// TODO: set text color on Every Other list item to gray

for (var i = 0; i < listItems.length; i++){
     if (i % 2 === 0){
         listItems[i].style.color = "red";
     }

     if (listItems[i].hasAttribute("data-dbid")){
         
         if (listItems[i].getAttribute("data-dbid") === "1"){
             listItems[i].style.color = "blue";
         } 
     
     }
     
}

//change the content for the sub-paragraph to something different....

var subParagraph = document.getElementsByClassName("sub-paragraph");
    console.log(subParagraph);
    
// subParagraph.innerHTML = "This is new content for the paragraph.";
// doesn't work because we didn't specifiy which item of the sub-paragraph we want to modify

subParagraph.item(0).innerHTML = "This is new content for the paragraph.";

// TODO: set the text color of li with data-dbid = 1 to blue


//converting a string to an array

var planetsString = "Mercury|Venus|Earth|Mars|Jupiter|Saturn|Uranus|Neptune";
var planetsArray = planetsString.split("|");

    console.log(planetsArray);
    console.log(planetsString);

// taking planetsArray and putting it in an unordered list
    
    var html = "<ul><li>";
    html += planetsArray.join("</li><li>");
    html += "</li></ul>";

    
    
//   document.body.innerHTML = html;  ******** This will replace EVERYTHING in the HTML *******


// unordered list and adding it to html element with id of planets.
var planetsList = document.getElementById("planets");
planetsList.innerHTML = html;












