/* JAVASCRIPT OBJECTS */
// Complete the following in a new js file named objects-exercise.js
"use strict";
/**
 * TODO: Create an object called movie. Include four properties
 *  Console log each property.
 */

// var movie = {
//     genre: "Horror",
//     writer: "John Carpenter",
//     director: "John Carpenter",
//     actress: "Jamie Lee Curtis"
// };
//
// console.log(movie.actress);
// console.log(movie.director);
// console.log(movie.writer);
// console.log(movie.genre);

/**
 * TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre
 *  console log each property.
 */

//  var movie = {
//      title: "Scream",
//      protagonist: "Sidney Prescott",
//      antagonist: "Ghostface",
//      genre: "Horror"
// };
//
// console.log(movie.antagonist);
// console.log(movie.protagonist);
// console.log(movie.title);
// console.log(movie.genre);


/**
 * TODO: Create a pet/companion object include the following properties:
 *  string name, number age, string species, number valueInDollars,
 *  array nickNames, vaccinations: date, type, description.
 */

// var pet = {
//     name: "Blue",
//     age: 1,
//     species: "Cane Corso",
//     valueInDollars: 5000,
//     nickNames: ["Boo", "Bootyroo", "Booskiroodledoo"],
//     vaccinations: {
//         date: "1 May 2020",
//         type: "Canine Parvovirus",
//         description: "1st vaccination for Parvovirus"
//     }
// };


/**
 * TODO: Add 3 additional objects to the array below called movies. Be sure to include five properties: title, releaseDate, director, genre, and myReview.
 *  The myReview property should be a function
 */

//EX
const movies = [
    {
        title: 'Batman',
        releaseDate: {
            month: "June",
            date: 23,
            year: 1989
        },
        director: {
            firstName: 'Tim',
            lastName: 'Burton'
        },
        genre: ['Action', 'Suspense', 'Superhero'],
        myReview: function () {
            console.log('This is Batman! Michael Keaton is Batman! 4/4 stars!');
        }

    },
    {
        title: "Spider-Man: Into the Spider-Verse",
        releaseDate: {
            month: "December",
            date: 10,
            year: 2020
        },
        director: {
            firstName: "Peter",
            lastName: "Ramsey",
        },
        genre: ["Action", "Adventure"],
        myReview:function () {
           console.log("One of my favorite Spider-Man movies to date. 4/4 stars!"); 
        }
    },
    {
        title: "Paranormal Activity",
        releaseDate: {
            month: "October",
            date: 13,
            year: 2017
        },
        director: {
            firstName: "Oren",
            lastName: "Peli",
        },
        genre: ["Horror", "Mystery"],
        myReview: function () {
           console.log("Scared the crap out of me!  Definitely a must see!!"); 
        }
    },
    {
        title: "The Holiday",
        releaseDate: {
            month: "November",
            date: 29,
            year: 2007
        },
        director: {
            firstName: "Nancy",
            lastName: "Meyers",
        },
        genre: ["Romance", "Comedy"],
        myReview: function () {
           console.log("Sweet movie every girl can relate too! I watch it every year around Christmas!"); 
        }
    }

    ];
 // Once you added the additional movies, complete the following:

// Loop through the array to display and console log the 'Release Date: month / date / year'.

// movies.forEach(function (movie) {
//       console.log(`Release Date: ${movie.releaseDate.month} / ${movie.releaseDate.date} / ${movie.releaseDate.year}`);
// });

//  *  Loop through the array to display and console log the 'Title, ReleaseDate, Director, myReview'

// movies.forEach(function (movie) {
//        console.log(movie.title);
//        console.log(movie.releaseDate);
//        console.log(movie.director);
//        console.log(movie.myReview());
// });

// OR
//  for (var i = 0; i < movies.length; i++){
//      console.log(`Title: ${movies[i].title}\nRelease Date: ${movies[i].releaseDate}\n
//      Director: ${movies[i].director.firstName}\nReview: ${movies[i].myReview()}`)
//  }




//  * Loop through the array to display and console log the 'Genre'

// movies.forEach(function (movie) {
//      console.log(movie.genre);
// });

// *  Loop through the array to display and console log the 'Director: FirstName LastName'

// movies.forEach(function (movie) {
//       console.log(movie.director);
// });


// BONUS
/** TODO:
 * HEB has an offer for the shoppers that buy products amounting to
 * more than $200. If a shopper spends more than $200, they get a 12%
 * discount. Write a JS program, using conditionals, that logs to the
 * browser, how much Stephen, Justin and Leslie need to pay. We know that
 * Justin bought $180, Stephen $250 and Leslie $320. Your program will have to
 * display a line with the name of the person, the amount before the
 * discount, the discount, if any, and the amount after the discount.
 *
 * Uncomment the lines below to create an array of objects where each object
 * represents one shopper. Use a foreach loop to iterate through the array,
 * and console.log the relevant messages for each person
 */

var shoppers = [
    {
        name: "Justin",
        amount: 180
    },
    {
        name: "Stephen",
        amount: 250
    },    
    {
        name: "Leslie",
        amount: 320
    }

    ];

function calculateDiscount(amount) {
      var discount = 0;
      var finalAmount = 0;
    if (amount > 200) {
        discount = amount * .12;
        finalAmount = amount - discount;
        
    } else {
        finalAmount = amount
    }
    return {
        
    }
}
shoppers.forEach(function (shopper) {
    console.log(shopper.name);
    console.log(shopper.amount);
    console.log(shopper.discount);
    console.log(total(shopper.totalAmount));
})

