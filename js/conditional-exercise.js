"use strict";
/**
 * TODO:
 * Write some JavaScript that uses a 'confirm' dialog to ask the user if they would like to enter a number. If they click 'Ok', prompt the user for a number, then use 3 separate alerts to tell the user:
 * - Whether the number is even or odd
 * - What the number plus 100 is
 * - If the number is negative or positive
 * If the user doesn't enter a number, use an alert to tell them that, and do not display any of the information above
 **/

  if (confirm("Would you like to enter a number?") === true) {
      console.log("okay");
      var input = prompt("Enter a number:");

      //if the user doesn't enter a number
      if (isNaN(input)) {
          alert("Not a number!");
      } 
      else {
          if (input % 2 === 0) {
              alert(input + " is even!");
          } 
          else {
              alert(input + " is odd!");
          }
          //takes input and adds 100 to it
          alert(input + " + 100 is " + (Number(input) + 100));

          //checks to see if input is neg or pos
          if (input >= 0) {
              alert(input + " is a positive number");
          } 
          else {
              alert(input + " is a negative number");
          }
      }
  }    
  else {
      alert("Okay, good-bye");
  }
  
  
/**
 * TODO:
 * Create a function named 'color' that accepts a string that is a color name as an input. This function should return a message that related to that color. Only worry about the colors defined below, if the color passed is not one of the ones defined below, return a message that says so.
 * Ex
 * color('blue') // returns "blue is the color of the sky"
 * color('red') // returns "Roses are red"
 * You should use an if-else if- else block to return different messages.
 */
function color(input){
    if (input === "blue"){
        alert("Blue is the color of the sky!");
    }    
    else if (input === "red"){
        alert("Roses are red!");
    }
    else {
        alert("That is a unique color!");
    }
}

color("blue");


/**
 * TODO:
 * It's the year 2021, it's fall, and it's safe to shop again!
 * Suppose there's a promotion at Target, each customer is given a randomly generated "lucky Target number" between 0 and 5. If your lucky number is 0, you have no discount, if your lucky number is 1 you'll get a 10% discount, 2 is a 25% discount, 3 is a 35% discount, 4 is a 50% discount, and 5 you'll get a 75% discount.
 * Write a function named 'calculateTotal' that accepts a lucky number and total amount, and returns the discounted price.
 * Ex
 * calculateTotal(0, 100) // returns 100
 * calculateTotal(4, 100) // return 50
 * Test your function by passing it various values and checking for the expected return value.
 */
function calculateTotal(luckyNumber, totalAmount){
    if (luckyNumber === 0){
       alert(totalAmount);
   } 
   else if (luckyNumber === 1){
       // alert(totalAmount * .90);
       alert(totalAmount - (totalAmount * .10));
   } 
   else if (luckyNumber === 2){
       alert(totalAmount * .75);
   }
   else if (luckyNumber === 3){
       alert(totalAmount * .65);
   }
   else if (luckyNumber === 4){
       alert(totalAmount * .50);
   }
   else if (luckyNumber === 5){
       alert(totalAmount * .25);
   }
}

calculateTotal(5, 100)








/**
 * TODO:
 * Create a grading system using if/else if/ else statements
 // BONUS Use the switch statements
 // 100 - 97 A+
 // 96 - 94 A
 // 93 - 90 A -
 // 89 - 87 B +
 // 86 - 84 B
 // 83 - 80 B -
 // 79 - 77 C +
 // 76 - 74 C
 // 73 - 70 C -
 // 69 - 67 D +
 // 66 - 64 D
 // 63 - 60 D -
 // < 59 = F
 */

function gradeScale(grade) {   //could also do:  var grade = prompt ("Enter your grade: ");


    if (grade >= 97 && grade <= 100)
        console.log("A+");

    else if (grade >= 94 && grade <= 96)
        console.log("A");

    else if (grade >= 90 && grade <= 93)
        console.log("A-");

    else if (grade >= 87 && grade <= 89)
        console.log("B+");

    else if (grade >= 84 && grade <= 86)
        console.log("B");

    else if (grade >= 80 && grade <= 83)
        console.log("C-");

    else if (grade >= 77 && grade <= 79)
        console.log("C+");

    else if (grade >= 74 && grade <= 76)
        console.log("C");

    else if (grade >= 70 && grade <= 73)
        console.log("C-");

    else if (grade >= 67 && grade <= 69)
        console.log("D+");

    else if (grade >= 64 && grade <= 66)
        console.log("D");

    else if (grade >= 60 && grade <= 63)
        console.log("D-");

    else {
        console.log("F");
    }
}
   gradeScale(97)
   
/**
 // * TODO:
 * Create a time system (MILITARY TIME) using if/else if/else statement
 * OR switch case
 * < 12 = 'Good morning'
 * < 18 = 'Good afternoon'
 * something for 'Good evening
 **/
        

function time(x) {

    if (x >= 600 && x <= 1159)
        console.log("Good Morning");

    else if (x >= 1200 && x <= 1659)
        console.log("Good Afternoon");

    else if (x >= 1700 && x <= 2100)
        console.log("Good Evening");
}

time()
    
/**
 * TODO:
 * Create a season system using if/else if/else statement
 * EX. 'December - February = Winter', etc.
 **/
 
        let month = "Apr"   // could also do function seasons(month){

            if (month === "Dec" || month === "Jan" || month === "Feb"){
                console.log("Winter");    // could also use alert 
            }

            else if (month === "Mar" || month === "Apr" || month === "May"){
                console.log("Spring");
            } 

            else if (month === "Jun" || month === "Jul" || month === "Aug"){
                console.log("Summer");
            } 

            else if (month === "Sep" || month === "Oct" || month === "Nov"){
                console.log("Fall");
            }
            
           




