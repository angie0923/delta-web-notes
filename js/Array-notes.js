/*
ARRAYS
-data structure that holds an ordered list of items.
-a variable that contains a list of values

SYNTAX:

[] - an empty array

var color = ["red"]; - 1 element
var color = [ "red", "white", "blue"]; - 3 elements

ALTERNATIVE WAY:

using the Array constructor.....

var nameOfVariable = new Array();

var color = new Array ("red");
var colors = new Array("red", "white", "blue");
  ****** DO NOT FORGET ABOUT SEPARATING EACH ELEMENT'S NAME WITH A COMMA ******


COUNTING ARRAY ITEMS

To find out how many items are in an array, we
can use the "length" property.

var shapes = ["square", "rectangle", "circle"];
console.log(shapes.length);

output: 3

the length property of an array returns the length of an array
(the number of array elements)


ACCESSING ARRAY ELEMENTS

Once you have an array of values, you will probably want to use the data inside
the array.

var shapes = ["square", "rectangle", "circle"];

To access an element in an array, you specify an index in the square brackets[]:

arrayName[index];

Arrays are ZERO-INDEXED, meaning the first slot in an array 
in actually #0.


shapes[0];        square

shapes [1];       rectangle

shapes [2];      circle


ITERATING ARRAYS

When you ITERATE over an array, it means to cycle or LOOP through the elements
of the array.

We can use our looping statements to iterate through arrays.

var shapes = ["square", "rectangle", "circle"];

loop through the array and log the values.

for loop
for (var i=0; i < shapes.length; i++){
console.log ("the shape at index "+ i + "is: " + shapes[i]);
}

Here we are constructing a for loop that starts at ZERO (this index of the 
first element in array).

it ends at the length of the array minus one ( the index of the last 
element in any array).

ITERATING ARRAYS WITH FOR-EACH LOOP

A forEach method can be used as an alternative to a for loop

the forEach method on an array takes another function as a parameter
and doesn't have a return value.

The function passed to forEach takes in up to 3 parameters that will provide
access to the array element

This function is referred to as a callback function.

SYNTAX for forEach:

nameOfArray.forEach(function(element,index,array){
    // ...
    
});

example:

var shapes = ["square", "rectangle", "circle"];
 shapes.forEach(function(shape){
    console.log("shape name: "+ shape);
    
});

forEach Loop

var shapes = ["square", "rectangle", "circle"];

shapes.forEach(function(shape){
console.log("Shape name: " + shape);

Good habits when using forEach:

-Name your arrays as a plural noun
-refer to individual elements by the singular version of the noun,
-omit any parameters you don't intend to use.

in the ex
-used an array named shapes.
-defined the parameter name of the .forEach callback as shape.

CHANGING / MANIPULATING ARRAYS

JavaScript arrays have a variety of methods that allows for manipulation
of the array.

most common:

.push() - add an item to the end of an array
.unshift() - adds an item to the beginning of an array
.pop() - removes the last item of an array
.shift() - removes the first item of an array
.indexOf() -  returns the first occurrence of what you're looking for
.slice() - copies a portion of that array, returns a new array but does not
modify the original

.reverse() - reverses the original array
.sort() - convert items in the array to their string equivalent and order
them based on that value.

 */


// JavaScript Arrays

// Basic syntax
// [] - an empty array

var myArray = ["red", 12, -144, true];
//     console.log(myArray);
//     console.log(myArray.length);  // 4
//
// console.log(myArray[2]);  // -144
//
// console.log(myArray[4]);  // undefined
    
// ITERATING AN ARRAY

var delta = ["jose", "alyssa", "victor", "angela"];

// there are __ members in Delta Cohort

// console.log("There are " + delta.length + " members in Delta Cohort");


// Loop through the array and console.log the values

for(var i = 0; i < delta.length; i++ ){
    // print out all the values for delta individually
    // console.log(delta[i]);
}

// forEach loop

/*
SYNTAX
nameOfArray.forEach(function ( element, index, array) {
      .. run code
 });     

 */

// example

var ratings = [25, 34, 57, 62, 78, 89, 91];

// ratings.forEach(function (rating) {
//         console.log(rating);
// });

var pizzas = ["cheese", "dough", "sauce", "sausage", "pepperoni"];
    
// pizzas.forEach(function (pizza) {
//         console.log(pizza);
// });

// CHANGING / MANIPULATING ARRAYS

var fruits = ["apple", "grape", "orange", "watermelon"];
       console.log(fruits);
//       
// //       add to the array
//
// fruits.push("strawberry");
// console.log(fruits);
//
// fruits.unshift("dragon fruit");
// console.log(fruits);
//
// fruits.push("cherry", "blueberry", "pineapple");
// console.log(fruits)


// REMOVE FROM ARRAY
// remove the last element of the array
// fruits.pop();
//    console.log(fruits);
   
//   remove the first element of the array
// fruits.shift();
//       console.log(fruits);

// LOCATING ARRAY ELEMENT(S)

var indexElement = fruits.indexOf("orange");
// console.log(indexElement);    // 2
    
// SLICING

// .SLICE () copies a portion of the original array

var slice = fruits.slice(1, 3);
// console.log(slice);
// console.log(fruits);

// REVERSE

fruits.reverse();
// console.log(fruits);

// SORT

// console.log(fruits.sort());

































