/* JAVASCRIPT DOM */
// NOTE: You'll need to create an html to link this js file, along with creating some content in the html
"use strict";


/** When the button with the id of `change-bg-color` is clicked the background of
 the page should turn blue.*/



var myButton = document.getElementById("change-bg-color");
function changeColor() {
    if (document.body.style.backgroundColor === "lightBlue") {
        document.body.style.backgroundColor = "white";
    }
    else{
        document.body.style.backgroundColor = "lightBlue";
    }
}
myButton.addEventListener("click", changeColor);


/** When the button with an id of `append-to-ul` is clicked, append an li with
 the content of `text` to the ul with the id of `append-to-me`.*/


var clickMe = document.getElementById("append-to-ul");


function addText() {
    //create a var to target the #append-to-me
    var list = document.getElementById("append-to-me");
    // create a var to create the element `li` that will be added to the ul
    var li = document.createElement("li");
   // adding content "text" to the li variable
    li.appendChild(document.createTextNode("text"));
    // add the variable `li` to the ul
    list.appendChild(li);    
}
clickMe.addEventListener("click", addText);


/** Ten seconds after the page loads, the heading with the id of `message` should
 change it's text to "Goodbye, World!". Completed*/

var heading = document.getElementById("message");
setTimeout(function () {
   heading.innerHTML = "Goodbye World!" 
}, 10000);

/** When a list item inside of the ul with the id of `hl-toggle` is first
 clicked, the background of the li that was clicked should change to
 yellow.
 When a list item that has a yellow background is clicked, the
 background should change back to the original background.
 */
var hlItem = document.getElementById('hl-toggle');
function highlightItem() {
    if (hlItem.style.backgroundColor === 'yellow') {
        hlItem.style.backgroundColor = 'white';
    } else {
        hlItem.style.backgroundColor = 'yellow';
    }
}
hlItem.addEventListener('click',highlightItem);


/** When the button with the id of `upcase-name` is clicked, the element with the
 id of `output` should display the text "Your name uppercased is: " + the
 value of the `input` element with the id of `input` transformed to uppercase.*/

var outputText = document.getElementById("output");
var inputText  = document.getElementById("input");
var upperCaseBtn = document.getElementById("upcase-name");

function convertInput() {
   // declare a variable for the userInput value and uppercase
   var userInputUpcase = inputText.value.toUpperCase();
   
   // calling output text variable and displaying by attaching the .innerHTML
   outputText.innerHTML = "Your name uppercased is: " + userInputUpcase;
}

upperCaseBtn.addEventListener("click", convertInput);


/** Whenever a list item inside of the ul with the id of `font-grow` is _double_
 clicked, the font size of the list item that was clicked should double.*/
var growItem = document.getElementById('font-grow');
function doubleSize() {
    growItem.style.fontSize = '200%';
}
growItem.addEventListener('dblclick',doubleSize);








