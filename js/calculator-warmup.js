

var body = document.getElementById("body");
body.style.backgroundColor = "lightBlue"


function sphereVolume() {
   var volume;
   var radius = document.getElementById("radius").value;
   
   radius = Math.abs(radius);
   
   volume = (4/3) * Math.PI * Math.pow(radius,3);
   volume = volume.toFixed(4);
   
   var answer = document.getElementById("answer");
   answer.innerHTML = volume;
   
   return false;
}

window.onload = document.getElementById("MyForm").onsubmit = sphereVolume;












