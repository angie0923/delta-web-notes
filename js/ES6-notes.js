"use strict";

// ES6

//  EXPONENT OPERATOR

//javascript

// Math.pow(2,5);
// console.log(Math.pow(2,5));  //32

// ES6

// console.log(2 ** 5);  // 32

// var vs let vs const

// let--blocked scope variable -- reassign values (ES6 var)
// const--blocked scope variable--can not be used to reassign values-- cannot use the variable name anywhere else
// var--global variable-- can be used to reassign values

// let instructor = "Stephen";

// instructor = "Justin";
//
// console.log(instructor) // gave us Justin rather than stephen

// const instructorTwo = "Stephen";
// if we did a console log here first then we get Stephen
// instructorTwo = "Justin";

// console.log(instructorTwo); // error message given--assignment to constant variable

// const age = 21;

// age += 1;  // this is not allowed, we have already declared age above.


// TEMPLATE STRINGS VS CONCATENATION

// concatenation uses "" and +
// const cohort = "Delta";
// console.log("Hello, and welcome to " + cohort + "!");
//
// // template strings uses backtick with $ and curly braces
//
// console.log(`Hello, and welcome to ${cohort}!`);

// for .... of

// SYNTAX for .... of
/*

for (let element of interable){

}
*/

// const arr = ["one", "two", "three"];
//
// for (let e of arr){
//     console.log(e);
// }

// example above in a for loop?

// for (var i = 0; i < arr.length; i ++){
//     console.log(arr[i]);
// }


//  TODO: using a for of loop, log each day of the week

// const days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
//  for (let e of days){
//      console.log(e);
//  //    to have days onto the html web page......we also have to put a div w/an id of days in html
//     let daysOnHTML = "";
//
//     daysOnHTML += `
//         <ul>
//                 <li>${e}</li>
//
//         </ul>
//         `;
//
//     $("#days").append(daysOnHTML);
//  }



// OR

// const days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
//
// let daysOnHTML = "";

//  for (let e of days){
//     console.log(e);
//     //    to have days onto the html web page......we also have to put a div w/an id of days in html
//
//
//     daysOnHTML += `
//         <ul>
//                 <li>${e}</li>
//
//         </ul>
//         `;
//
//     $("#days").html(daysOnHTML);
// }


//  ARROW FUNCTIONS

// shorthand function syntax:

// js

// function sayHello(name) {
//     return `hello ${name}`;
// }
//
// console.log(sayHello("Delta"));


// es6 arrow function

// let sayHelloAgain = name => `Hello ${name}`;
// console.log(sayHelloAgain("Delta"));

// js function w/ no parameters

// function returnTwo() {
//     return 2;
// }
// console.log(returnTwo());

// es6 arrow function with no parameters

// let returnTwoAgain = () => 2;
// console.log(returnTwoAgain());



// DEFAULT FUNCTION PARAMETER VALUES

// typeof --- tells you they type of data

// var x = 1;
//
// console.log(typeof x);  // number
//
// var y = "Delta";
// console.log(typeof y);  // string
//
// var z = true;
// console.log(typeof z);  // boolean
//
// var a;
// console.log(typeof a);  // undefined

// JavaScript
// function greeting(cohort) {
//    if (typeof cohort === "undefined"){
//        cohort = "World";
//    }
//    return console.log(`Hello ${cohort}`);
// }
//
// greeting();  // Hello World
//
// greeting("Delta");

// const greetingAgain = (cohort = "World") => console.log(`Hello ${cohort}`);

// greetingAgain(); // Hello World
// greetingAgain("Echo"); //Hello Echo



// second example of default parameters

// var addArgA = (num1, num2) => {
//     if (num1 === undefined){
//         num1 = 2;
//     }
//     if (num2 === undefined){
//         num2 = 2;
//     }
//     return num1 + num2;
// };

// console.log(addArgA()); // 4
// console.log(addArgA(1)); // 1 + 2 = 3
// console.log(addArgA(5,9)); // 14

// OBJECT PROPERTY VARIABLE ASSIGNMENT SHORTHAND

// objects from JS

// var person = {
//     name: "Delta",
//     age: 2021
// };

// objects from es6 shorthand

// const name = "Delta";
// const age = 2021;

// const person = {           //assign properties first then put in obj
//     name,
//     age
// };



// OBJECT DESTRUCTURING
//shorthand for creating variables from object properties

// js
// var person = {name: "Delta", age: 2}; //  object
// var personName = person.name; // Delta
// var personAge = person.age; // 2


// ES6

// const person = {name: "Delta", age: 2};
// const {name, age} = person;

// DESTRUCTURING ARRAYS
// CREATING NAMES FOR INDEXES INSIDE AN ARRAY

// var myArray = [1, 2, 3, 4, 5];

// const [x, y, thirdIndex, bob, adam] = myArray;  //create a name for each index ( doesn't matter what the name is)

// console.log(bob);


//===================================

// ARROW FUNCTION DRILLS

// TODO: CREATE A ARROW FUNCTION THAT'LL TAKE IN A 
//LENGTH AND WIDTH AND RETURNS THE AREA

// js
// function area(length, width) {
//     return length * width;
// }
//
// console.log(area(10,20)); // 200
//
//
// const areaAgain = (length, width) => length * width;
// console.log(areaAgain(10,20));

// TODO:  create an arrow function that'll take in a
// length and width, and returns the perimeter.

// const perimeter = (width, height) => (2 * width) + (2 * height);
// console.log(perimeter(6,2)); // 16


// TODO: convert js function into arrow function

// function helloCohort(greeting, cohort) {
//     if (typeof greeting === "undefined"){
//         greeting = "Good Day";
//     }
//     if (typeof  cohort === "undefined"){
//         cohort = "Delta";
//     }
//
//     return greeting + " " + cohort;
// }


const helloCohortAgain = (greeting = "Good Day", cohort = "Delta") => `${greeting} ${cohort}`;

console.log(helloCohortAgain());