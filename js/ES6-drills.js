"use strict";



/* ES6 DRILLS */
// Complete the following in a new js file named 'es6-drills.js'
const petName = 'Chula';
const age = 14;

// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');

console.log(`My dog is named ${petName} and she is ${age} years old.`)


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }

let addTen = num1 => num1 + 10;
    console.log(addTen(4));

    // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }

let minusFive = num1 => num1 - 5;
    console.log(minusFive(9));

    // REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }

let multiplyByTwo = num1 => num1 * 2;
    console.log(multiplyByTwo(4));

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };

const greetings = name => `Hello, ${name} how are you?`
    console.log(greetings("David"));


// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }


// let haveWeMet = name =>"Bob" ?  "Nice to see you again" : "Nice to meet you";
//     console.log(haveWeMet("David"))

// const haveWeMet = name => {
//     if (name === "Bob"){
//         return `${name}, Nice to see you again`
//     }
//     else {
//         return `Nice to meet you`
//     }
// }
// haveWeMet("David")

// OR

// Terniary operator
// syntax: condition ? message if condition is true : message if condition is not true

const haveWeMet = name => (name === "Bob") ? "Nice to see you again" : "Nice to meet you";
console.log(haveWeMet("David"))

// USING A FOR...OF, CONSOLE LOG THE SUM OF THE ARRAY scores
var scores = [80, 90, 75];
var total = 0;

for (let score of scores){
    total += score;

}

console.log(total);


// BONUS:
    // REWRITE THE FOLLOWING IN TEMPLATE STRINGS
var li = '<li>' +
    '<div class="row">' +
    '<div class="col-md-4">' +
    '<img src="' + moviePoster + '" height="250" alt="" />' +
    '</div>' +
    '<div class="col-md-8">' +
    '<h2>' + movieTitle + '</h2>' +
    '</div>' +
    '</div>' +
    '</li>';


var liWithTemplateString = `
    <li>
        <div class="row">
            <div class="col-md-4">
                <img src="${moviePoster}" height="250" alt="">
            
            </div>
            <div class="col-md-8">
                <h2>${movieTitle}</h2>            
            </div>
            
        </div>
    </li>
    ;`



