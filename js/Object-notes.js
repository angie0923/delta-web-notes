/*
OBJECTS

in JavaScript, an object is an unordered collection of key-value pairs.
Each key-value pair is called  a property.

Properties can be a string, and a value of properties can be any valid JS value
: string, number, array, and even a function.

New Object Instance
var car = new Object();

console.log(type of car); // "object"

Object Literal Notation

var car = {};

console.log(type of car); // "object"

SETTING PROPERTIES ON A CUSTION OBJECT

Properties are assigned to objects by using the dot notation or the 
array notation.

var car = {};  // empty object
car.make = "Toyota";
car["model"] = "4Runner";

*in general, the dot notation is more preferred method

Most common way to assign properties it the following:

var car = {

make: "Toyota",
model: "4Runner",

};

***** DON'T DO THIS: ******************

var car = {
"numberOfWheels": 4

};

**************************************

Accessing Properties on an Object

Object properties are accessed in the same way they are set,
either by the dot notation or the array notation.
                

console.log( "The car make is" +car.make);
console.log(`The car make is ${car.make}`);

console.log("The car model is" + car["model"]);  ** not commonly used **




Nested Values:

The value of a property can be any valid value in JavaScript,
including arrays, or other objects.

This allows us to create complex data structures like the following example
                              **    Array containing 2 objects **
var cars = [
{
    make: "Toyota",
    model: "4Runner",
    features: [ "heated seats", "bluetooth radio"],
    owner: {
        name: "john doe",
        age: 30
    }
},
{
    make: "Honda",
    model: "Civic",
    features: [ "FM/AM Radio", "Still runs"],
    owner: {
        name: "jane Doe",
        age: 27
    }
}

];



Assigning Functionality to an Object

Besides having data type properties, an object can also have functions,
known as object methods.

Creating a method on an object is much like
creating a property, except the property value is a function.

var car = {
    make: "Toyota",
    model: "4Runner",
    //creat a honk function on the car object
    honk: function (){
        alert("honk! honk!");
    }
};

// honk the horn
car.honk();        

The `this` keyword

In other languages such as PHP and Java, the 
"this" keyword is a reference to the current object.

In JavaScript, it can refer to a different object based on how 
a function is called.

`this` refers to the "owner" of the function

var car = {
     make: "Toyota",
    model: "4Runner",
    // create a showInfo function on the car object
    showCarInfo: function () {
        console.log ("Car make/model is: " + this.make + "" + this.model);
        }
};

// call the make/model

car.showCarInfo();
                


this is the car object that "owns" the showCarInfo function
this.make is the make property of this object.

Exercises:
Create an object named person.  Include 3 properties: firstName,
lastName, age

The property value types should be : string, string, and number respectively.


var person = {
    firstName: "Angie",
    lastName:"Alexander",
    age: 43

};

console.log(person.lastName);

Include a list (array) of hobbies to the person object.

   var person = {
    firstName: "Angie",
    lastName:"Alexander",
    age: 43,
    hobbies: [ "music", "movies", "sports"]
};

 */



// JavaScript Objects

// var car = {
//     make: "Toyota",
//     model: "4Runner",
//     color: "Hunter Green",
//     numberOfWheels: 5
// };
// console.log(car);

// get the value for color?

// console.log(car.color);    // Hunter Green


//  NESTED VALUES 

// var cars = [
//     {
//         make: "Toyota",
//         model: "4Runner",
//         color: "Hunter Green",
//         numberOfWheels: 5,
//         features: ["Heated seats", "Bluetooth Radio", "Automatic Doors"],
//         alarm: function () {
//             alert("Sound the alarm!");
//         }
//     },
//     {
//         make: "Honda",
//         model: "Civic",
//         color: "Black",
//         numberOfWheels: 4,
//         features: ["Great Gas Mileage", "AM/FM Radio", "Still Runs"],
//         alarm: function () {
//             alert("No alarm ... sorry!");
//         },
//         owner: {                        // object with in an object
//             name: "John Doe",
//             age: 35
//         }
//     },
//     {
//         make: "Ford",
//         model: "Bronco",
//         color: "Navy Blue",
//         numberOfWheels: 4,
//         features: ["Power Windows", "GPS Navigation", "4 Wheel Drive"],
//         alarm: function () {
//             alert("Move, get out the way!");
//         },
//         owner: {
//             name: "Jane Doe",
//             age: 30,
//             address: {
//                 street: "150 Alamo Plaza",
//                 city: "San Antonio",
//                 state: "Texas"
//             }
//         }
//     }
//
// ];

// Log the Honda object
// console.log(cars[1]);

//log the Toyota object
// console.log(cars[0]);

//log the Ford object
// console.log(cars[2]);

//  log the model of the Honda object only

// console.log(cars[1].model); // Civic

// log the owner of the Ford object

// console.log(cars[2].owner);

// log the city of the Ford owner

// console.log(cars[2].owner.address.city);    // San Antonio

// Log the SECOND feature of the Toyota object only

// console.log(cars[0].features[1]); // Bluetooth radio

// log the alarm function of the honda's object

// console.log(cars[1].alarm());  // Sorry.....no alarm

// var cars = [
//     {
//         make: "Toyota",
//         model: "4Runner",
//         color: "Hunter Green",
//         numberOfWheels: 5,
//         features: ["Heated seats", "Bluetooth Radio", "Automatic Doors"],
//         alarm: function () {
//             alert("Sound the alarm!");
//         }
//     },
//     {
//         make: "Honda",
//         model: "Civic",
//         color: "Black",
//         numberOfWheels: 4,
//         features: ["Great Gas Mileage", "AM/FM Radio", "Still Runs"],
//         alarm: function () {
//             alert("No alarm ... sorry!");
//         },
//         owner: {                        // object with in an object
//             name: "John Doe",
//             age: 35
//         }
//     },
//     {
//         make: "Ford",
//         model: "Bronco",
//         color: "Navy Blue",
//         numberOfWheels: 4,
//         features: ["Power Windows", "GPS Navigation", "4 Wheel Drive"],
//         alarm: function () {
//             alert("Move, get out the way!");
//         },
//         owner: {
//             name: "Jane Doe",
//             age: 30,
//             address: {
//                 street: "150 Alamo Plaze",
//                 city: "San Antonio",
//                 state: "Texas"
//             }
//         }
//     }
//
// ];

// display the features of all cars

// cars.forEach(function (car) {
//     car.features.forEach(function (feature) {
//          console.log(feature);
//     })
//    
// });

//  `this` keyword

// var videoGames = {};
//
// videoGames.name = "Super Mario";
//
// videoGames.company = "Nintendo";
//
// videoGames.genre = "Adventure";

// create a method on the videoGame object

// videoGames.description = 
//     function () {
//        alert("Video Game Name: " + this.name
//        +  "\nCompany of Video Name: " + this.company
//        +  "\nGenre of Video Game: " + this.genre); 
//     };

// call the description property

// videoGames.description();





























