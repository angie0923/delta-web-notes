var myString = "My pet's name is Blue.  He is a Cane Corso and 1 year old."

function count(input) {
       console.log(input.length);
}
count(myString);

function add(num1) {
    console.log(num1 + 7);
}
add(5);

/* function add(a + b) {
    console.log(a + b);
    }

add(a: 12, b:5);  17    
    
*/
function subtract(num2) {
    console.log(num2 - 3);    
}
subtract(7);

function multiply(num3) {
    console.log(num3 * 9);
}
multiply(9);

function divide(num4) {
     console.log(num4 / 9)
}
divide(81);

function remainder(num5) {
     console.log(num5 % 3);
}
remainder(-17);

function square(num6) {
    return num6 ** 2; // ES6 JavaScript
    // num6 * num6
}
console.log(square(9));

// ANONYMOUS FUNCTIONS
// Functions without a name, and stores it in a variable
/*
SYNTAX:

var nameOfVariable = function() { 
    .... code inside of function's body
    };

 */

// SQUARE ANON FUNCTION

var square = function (num) {
    console.log (num * num)
};

var result7th = square(5);












