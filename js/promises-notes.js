"use strict";

//Promises is a tool for handling asynchronous event

/*
three states of a promise

- pending -- hasn't occurred yet
- resolved -- event successfully occured
- rejected -- event happened but error occurred

 */

// example

// var isMomHappy = true;

// promise

// var willIGetANewPhone = new Promise(function (resolve, reject){
//     if (isMomHappy === true){
//         var phone = {
//             brand: "Samsung",
//             color: "Black"
//         };
//         resolve(phone);
//     }
//     else {
        // var reason = new Error("Your grades are not good!");
        // reject(reason);
    // }
// });

// console.log(willIGetANewPhone)


// var promise = new Promise(function (resolve, reject){
//     setTimeout(function () {
//         resolve("Promises is resolved")
//         // reject("Promise has been rejected")
//     }, 5000);
// });
//
// console.log(promise)


// let goodKid = true;

// create a promise

// const getsCake = new Promise(function (resolve, reject){
//     if (goodKid === true){
//         resolve("Here is a piece of chocolate cake");
//     }
//     else {
//         reject("No cake for you!");
//     }
// })
//
//     .then(data => console.log(data))
//     .catch(error => console.log(error));
//
// ;
//
// console.log(getsCake);


// make a request to the star wars api with promise

// $.get(`https://swapi.dev/api/people/1`)
//
//     // setting up a .then() if it resolves
//     .then(function (data) {
//         console.log(data)
//
//     })
//
//     // setting up a .catch() if it rejects
//     .catch(function (error) {
//         console.log(error)
//
//     })


const starbucksOrder = type => {


let makeOrder = new Promise( (resolve, reject) =>{
    setTimeout(function () {
        resolve(`Coffee type: ${type} is ready`);
    }, 2000)
});

// console.log(makeOrder)


let processOrder = new Promise((resolve, reject) => {
    setTimeout(function (){
        resolve(`Coffee type ${type} has been ordered and paid for.`)
    }, 4000);
});

let enjoy = new Promise((resolve, reject) => {
   setTimeout(function () {
      resolve(`Enjoy your ${type}!`)
   }, 6000);



})

// console.log(processOrder);

    return Promise.all([processOrder, makeOrder, enjoy]);
    // promise.all- accepts an array of promises and resolves when all of the individual promises
    // have been resolved.

}

starbucksOrder("espresso").then((data)=>{
    console.log(data[0]);
    console.log(data[1]);
    console.log(data[2]);
}).catch((error)=> console.log(error));











































