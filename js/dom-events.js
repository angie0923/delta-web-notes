// EVENT LISTENERS

/*
SYNTAX:
    target.addEventListener(type, listener, [useCapture *optional*])
    --Listener is the function that is called when an event happens on the target--
    

 */

/*
TYPE OF EVENTS
keyup
keydown
click
change
submit
mouseover
 */


var testButton = document.getElementById("testBtn");

// ADD EVENT LISTENER USING AN ANON FUNCTION

// testButton.addEventListener("click", function () {
//       if (document.body.style.backgroundColor === "red"){
//           document.body.style.backgroundColor = "white";
//       }
//       else{
//           document.body.style.backgroundColor = "red";
//       }
// });

// add even listener from a previously defined function

function toggleBackgroundColor() {
    if (document.body.style.backgroundColor === "red"){
        document.body.style.backgroundColor = "white";
    }
    else {
        document.body.style.backgroundColor = "red";
    }
}

testButton.addEventListener("click", toggleBackgroundColor);


// REGISTER ADDITIONAL EVENTS

//when a cursor hovers OVER a paragraph, change the color of the text.
// Also, change the font family and make the font larger.

var paragraph = document.querySelector("p");

// create a function that will change the color, font-family, and font size.

function makeParagraphChange() {
   paragraph.style.color = "green";
   paragraph.style.fontFamily = "Comic Sans Ms";
   paragraph.style.fontSize = "30px";
}

// add a mouseover event to the "p" that will trigger the makeParaghraphChange function

paragraph.addEventListener("mouseover", makeParagraphChange);

// create a function that will change the background color to "blue"

function changeBackgroundToBlue(e) {
     e.target.style.backgroundColor = "blue";
}
 var listItems = document.getElementsByTagName("li");
 for (var i = 0; i < listItems.length; i++) {
      listItems[i].addEventListener("click", changeBackgroundToBlue);
 }







